# Erich Schmids 10 Keys Braille  
# Table of Contents

1.  [Braille key numbers](#braille-key-numbers)
2.  [Navigation](#navigation)
3.  [Braille](#braille)
4.  [Modifier](#modifier)

# Braille key numbers

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col   />

<col  />
</colgroup>
<thead>
<tr>
<th scope="col">1</th>
<th scope="col">4</th>
</tr>


<tr>
<th scope="col">2</th>
<th scope="col">5</th>
</tr>


<tr>
<th scope="col">3</th>
<th scope="col">6</th>
</tr>


<tr>
<th scope="col">7</th>
<th scope="col">8</th>
</tr>
</thead>

<tbody>
<tr>
<td>9</td>
<td>10</td>
</tr>
</tbody>
</table>

9 and 10 are space, thumb keys  


<a id="orgfa72d5a"></a>

# Navigation

Chords with space (thumb keys) in Braille mode or without space in Navigation mode.  

-   Arrow up  
    1-space
-   Arrow left  
    2-space
-   Arrow down  
    4-space
-   Arrow right  
    5-space
-   Tabulator  
    4-5-space
-   Shift tabulator  
    1-2-space
-   Page up  
    2-3-7-space
-   Page down  
    5-6-7-space
-   Home  
    1-3-space
-   End  
    4-6-space
-   ESC  
    1-3-5-6-space
-   Delete  
    1-2-3-4-5-6-space
-   Ctrl backspace  
    1-2-3-4-5-6-7-space

# Braille

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<thead>
<tr>
<td>⠀</td>
<td>⠁</td>
<td>⠃</td>
<td>⠉</td>
<td>⠙</td>
<td>⠑</td>
<td>⠋</td>
<td>⠛</td>
<td>⠓</td>
<td>⠊</td>
<td>⠚</td>
<td>⠂</td>
<td>⠈</td>
<td>⠐</td>
<td>⠒</td>
<td>⠘</td>
</tr>


<tr>
<td>blank</td>
<td>1</td>
<td>12</td>
<td>14</td>
<td>145</td>
<td>15</td>
<td>124</td>
<td>1245</td>
<td>125</td>
<td>24</td>
<td>245</td>
<td>2</td>
<td>4</td>
<td>5</td>
<td>25</td>
<td>45</td>
</tr>


<tr>
<td>&#xa0;</td>
<td>a</td>
<td>b</td>
<td>c</td>
<td>d</td>
<td>e</td>
<td>f</td>
<td>g</td>
<td>h</td>
<td>i</td>
<td>j</td>
<td>,</td>
<td>"</td>
<td>!</td>
<td>:</td>
<td>></td>
</tr>
</thead>

<tbody>
<tr>
<td>⠄</td>
<td>⠅</td>
<td>⠇</td>
<td>⠍</td>
<td>⠝</td>
<td>⠕</td>
<td>⠏</td>
<td>⠟</td>
<td>⠗</td>
<td>⠎</td>
<td>⠞</td>
<td>⠆</td>
<td>⠌</td>
<td>⠔</td>
<td>⠖</td>
<td>⠜</td>
</tr>


<tr>
<td>3</td>
<td>13</td>
<td>123</td>
<td>134</td>
<td>1345</td>
<td>135</td>
<td>1234</td>
<td>12345</td>
<td>1235</td>
<td>234</td>
<td>2345</td>
<td>23</td>
<td>34</td>
<td>35</td>
<td>235</td>
<td>345</td>
</tr>


<tr>
<td>.</td>
<td>k</td>
<td>l</td>
<td>m</td>
<td>n</td>
<td>o</td>
<td>p</td>
<td>q</td>
<td>r</td>
<td>s</td>
<td>t</td>
<td>;</td>
<td>|</td>
<td>\*</td>
<td>+</td>
<td>\`</td>
</tr>
</tbody>

<tbody>
<tr>
<td>⠤</td>
<td>⠥</td>
<td>⠧</td>
<td>⠭</td>
<td>⠽</td>
<td>⠵</td>
<td>⠯</td>
<td>⠿</td>
<td>⠷</td>
<td>⠮</td>
<td>⠾</td>
<td>⠦</td>
<td>⠬</td>
<td>⠴</td>
<td>⠶</td>
<td>⠼</td>
</tr>


<tr>
<td>36</td>
<td>136</td>
<td>1236</td>
<td>1346</td>
<td>13456</td>
<td>1356</td>
<td>12346</td>
<td>123456</td>
<td>12356</td>
<td>2346</td>
<td>23456</td>
<td>236</td>
<td>346</td>
<td>356</td>
<td>2356</td>
<td>3456</td>
</tr>


<tr>
<td>-</td>
<td>u</td>
<td>v</td>
<td>x</td>
<td>y</td>
<td>z</td>
<td>&</td>
<td>%</td>
<td>{</td>
<td>~</td>
<td>}</td>
<td>(</td>
<td>0</td>
<td>)</td>
<td>=</td>
<td>#</td>
</tr>
</tbody>

<tbody>
<tr>
<td>⠠</td>
<td>⠡</td>
<td>⠣</td>
<td>⠩</td>
<td>⠹</td>
<td>⠱</td>
<td>⠫</td>
<td>⠻</td>
<td>⠳</td>
<td>⠪</td>
<td>⠺</td>
<td>⠢</td>
<td>⠨</td>
<td>⠰</td>
<td>⠲</td>
<td>⠸</td>
</tr>


<tr>
<td>6</td>
<td>16</td>
<td>126</td>
<td>146</td>
<td>1456</td>
<td>156</td>
<td>1246</td>
<td>12456</td>
<td>1256</td>
<td>246</td>
<td>2456</td>
<td>26</td>
<td>46</td>
<td>56</td>
<td>256</td>
<td>456</td>
</tr>


<tr>
<td>'</td>
<td>1</td>
<td>2</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>7</td>
<td>8</td>
<td>9</td>
<td>w</td>
<td>?</td>
<td>$</td>
<td><</td>
<td>/</td>
<td>\_</td>
</tr>
</tbody>

<tbody>
<tr>
<td>⡀</td>
<td>⡁</td>
<td>⡃</td>
<td>⡉</td>
<td>⡙</td>
<td>⡑</td>
<td>⡋</td>
<td>⡛</td>
<td>⡓</td>
<td>⡊</td>
<td>⡚</td>
<td>⡂</td>
<td>⡈</td>
<td>⡐</td>
<td>⡒</td>
<td>⡘</td>
</tr>


<tr>
<td>7</td>
<td>17</td>
<td>127</td>
<td>147</td>
<td>1457</td>
<td>157</td>
<td>1247</td>
<td>12457</td>
<td>1257</td>
<td>247</td>
<td>2457</td>
<td>27</td>
<td>47</td>
<td>57</td>
<td>257</td>
<td>457</td>
</tr>


<tr>
<td>bs</td>
<td>A</td>
<td>B</td>
<td>C</td>
<td>D</td>
<td>E</td>
<td>F</td>
<td>G</td>
<td>H</td>
<td>I</td>
<td>J</td>
<td>&#xa0;</td>
<td>’</td>
<td>mod</td>
<td>&#xa0;</td>
<td>€</td>
</tr>
</tbody>

<tbody>
<tr>
<td>⡄</td>
<td>⡅</td>
<td>⡇</td>
<td>⡍</td>
<td>⡝</td>
<td>⡕</td>
<td>⡏</td>
<td>⡟</td>
<td>⡗</td>
<td>⡎</td>
<td>⡞</td>
<td>⡆</td>
<td>⡌</td>
<td>⡔</td>
<td>⡖</td>
<td>⡜</td>
</tr>


<tr>
<td>37</td>
<td>137</td>
<td>1237</td>
<td>1347</td>
<td>13457</td>
<td>1357</td>
<td>12347</td>
<td>123457</td>
<td>12357</td>
<td>2347</td>
<td>23457</td>
<td>237</td>
<td>347</td>
<td>357</td>
<td>2357</td>
<td>3457</td>
</tr>


<tr>
<td>&#xa0;</td>
<td>K</td>
<td>L</td>
<td>M</td>
<td>N</td>
<td>O</td>
<td>P</td>
<td>Q</td>
<td>R</td>
<td>S</td>
<td>T</td>
<td>&#xa0;</td>
<td>\</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>@</td>
</tr>
</tbody>

<tbody>
<tr>
<td>⡤</td>
<td>⡥</td>
<td>⡧</td>
<td>⡭</td>
<td>⡽</td>
<td>⡵</td>
<td>⡯</td>
<td>⡿</td>
<td>⡷</td>
<td>⡮</td>
<td>⡾</td>
<td>⡦</td>
<td>⡬</td>
<td>⡴</td>
<td>⡶</td>
<td>⡼</td>
</tr>


<tr>
<td>367</td>
<td>1367</td>
<td>12367</td>
<td>13467</td>
<td>134567</td>
<td>13567</td>
<td>123467</td>
<td>1234567</td>
<td>123567</td>
<td>23467</td>
<td>234567</td>
<td>2367</td>
<td>3467</td>
<td>3567</td>
<td>23567</td>
<td>34567</td>
</tr>


<tr>
<td>&#xa0;</td>
<td>U</td>
<td>V</td>
<td>X</td>
<td>Y</td>
<td>Z</td>
<td>Ç</td>
<td>É</td>
<td>[</td>
<td>^</td>
<td>]</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
</tr>
</tbody>

<tbody>
<tr>
<td>⡠</td>
<td>⡡</td>
<td>⡣</td>
<td>⡩</td>
<td>⡹</td>
<td>⡱</td>
<td>⡫</td>
<td>⡻</td>
<td>⡳</td>
<td>⡪</td>
<td>⡺</td>
<td>⡢</td>
<td>⡨</td>
<td>⡰</td>
<td>⡲</td>
<td>⡸</td>
</tr>


<tr>
<td>67</td>
<td>167</td>
<td>1267</td>
<td>1467</td>
<td>14567</td>
<td>1567</td>
<td>12467</td>
<td>124567</td>
<td>12567</td>
<td>2467</td>
<td>24567</td>
<td>267</td>
<td>467</td>
<td>567</td>
<td>2567</td>
<td>4567</td>
</tr>


<tr>
<td>&#xa0;</td>
<td>Â</td>
<td>Ê</td>
<td>Î</td>
<td>Ô</td>
<td>Û</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>W</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>Ä</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
</tr>
</tbody>

<tbody>
<tr>
<td>⣀</td>
<td>⣁</td>
<td>⣃</td>
<td>⣉</td>
<td>⣙</td>
<td>⣑</td>
<td>⣋</td>
<td>⣛</td>
<td>⣓</td>
<td>⣊</td>
<td>⣚</td>
<td>⣂</td>
<td>⣈</td>
<td>⣐</td>
<td>⣒</td>
<td>⣘</td>
</tr>


<tr>
<td>78</td>
<td>178</td>
<td>1278</td>
<td>1478</td>
<td>14578</td>
<td>1578</td>
<td>12478</td>
<td>124578</td>
<td>12578</td>
<td>2478</td>
<td>24578</td>
<td>278</td>
<td>478</td>
<td>578</td>
<td>2578</td>
<td>4578</td>
</tr>


<tr>
<td>cr</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>Ò</td>
<td>&#xa0;</td>
<td>»</td>
</tr>
</tbody>

<tbody>
<tr>
<td>⣄</td>
<td>⣅</td>
<td>⣇</td>
<td>⣍</td>
<td>⣝</td>
<td>⣕</td>
<td>⣏</td>
<td>⣟</td>
<td>⣗</td>
<td>⣎</td>
<td>⣞</td>
<td>⣆</td>
<td>⣌</td>
<td>⣔</td>
<td>⣖</td>
<td>⣜</td>
</tr>


<tr>
<td>378</td>
<td>1378</td>
<td>12378</td>
<td>13478</td>
<td>134578</td>
<td>13578</td>
<td>123478</td>
<td>1234578</td>
<td>123578</td>
<td>23478</td>
<td>234578</td>
<td>2378</td>
<td>3478</td>
<td>3578</td>
<td>23578</td>
<td>34578</td>
</tr>


<tr>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
</tr>
</tbody>

<tbody>
<tr>
<td>⣤</td>
<td>⣥</td>
<td>⣧</td>
<td>⣭</td>
<td>⣽</td>
<td>⣵</td>
<td>⣯</td>
<td>⣿</td>
<td>⣷</td>
<td>⣮</td>
<td>⣾</td>
<td>⣦</td>
<td>⣬</td>
<td>⣴</td>
<td>⣶</td>
<td>⣼</td>
</tr>


<tr>
<td>3678</td>
<td>13678</td>
<td>123678</td>
<td>134678</td>
<td>1345678</td>
<td>135678</td>
<td>1234678</td>
<td>12345678</td>
<td>1235678</td>
<td>234678</td>
<td>2345678</td>
<td>23678</td>
<td>34678</td>
<td>35678</td>
<td>235678</td>
<td>345678</td>
</tr>


<tr>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>ç</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>À</td>
<td>&#xa0;</td>
<td>Ù</td>
<td>&#xa0;</td>
<td>ẞ</td>
</tr>
</tbody>

<tbody>
<tr>
<td>⣠</td>
<td>⣡</td>
<td>⣣</td>
<td>⣩</td>
<td>⣹</td>
<td>⣱</td>
<td>⣫</td>
<td>⣻</td>
<td>⣳</td>
<td>⣪</td>
<td>⣺</td>
<td>⣢</td>
<td>⣨</td>
<td>⣰</td>
<td>⣲</td>
<td>⣸</td>
</tr>


<tr>
<td>678</td>
<td>1678</td>
<td>12678</td>
<td>14678</td>
<td>145678</td>
<td>15678</td>
<td>124678</td>
<td>1245678</td>
<td>125678</td>
<td>24678</td>
<td>245678</td>
<td>2678</td>
<td>4678</td>
<td>5678</td>
<td>25678</td>
<td>45678</td>
</tr>


<tr>
<td>&#xa0;</td>
<td>â</td>
<td>ê</td>
<td>î</td>
<td>ô</td>
<td>û</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>«</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
</tr>
</tbody>

<tbody>
<tr>
<td>⢀</td>
<td>⢁</td>
<td>⢃</td>
<td>⢉</td>
<td>⢙</td>
<td>⢑</td>
<td>⢋</td>
<td>⢛</td>
<td>⢓</td>
<td>⢊</td>
<td>⢚</td>
<td>⢂</td>
<td>⢈</td>
<td>⢐</td>
<td>⢒</td>
<td>⢘</td>
</tr>


<tr>
<td>8</td>
<td>18</td>
<td>128</td>
<td>148</td>
<td>1458</td>
<td>158</td>
<td>1248</td>
<td>12458</td>
<td>1258</td>
<td>248</td>
<td>2458</td>
<td>28</td>
<td>48</td>
<td>58</td>
<td>258</td>
<td>458</td>
</tr>


<tr>
<td>sp</td>
<td>ht</td>
<td>sht</td>
<td>³</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>shift</td>
<td>&#xa0;</td>
<td>ar</td>
<td>al</td>
<td>&#xa0;</td>
</tr>
</tbody>

<tbody>
<tr>
<td>⢄</td>
<td>⢅</td>
<td>⢇</td>
<td>⢍</td>
<td>⢝</td>
<td>⢕</td>
<td>⢏</td>
<td>⢟</td>
<td>⢗</td>
<td>⢎</td>
<td>⢞</td>
<td>⢆</td>
<td>⢌</td>
<td>⢔</td>
<td>⢖</td>
<td>⢜</td>
</tr>


<tr>
<td>38</td>
<td>138</td>
<td>1238</td>
<td>1348</td>
<td>13458</td>
<td>1358</td>
<td>12348</td>
<td>123458</td>
<td>12358</td>
<td>2348</td>
<td>23458</td>
<td>238</td>
<td>348</td>
<td>358</td>
<td>2358</td>
<td>3458</td>
</tr>


<tr>
<td>ctrl</td>
<td>“</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>sctrl</td>
<td>&#xa0;</td>
<td>Ö</td>
<td>&#xa0;</td>
<td>ä</td>
</tr>
</tbody>

<tbody>
<tr>
<td>⢤</td>
<td>⢥</td>
<td>⢧</td>
<td>⢭</td>
<td>⢽</td>
<td>⢵</td>
<td>⢯</td>
<td>⢿</td>
<td>⢷</td>
<td>⢮</td>
<td>⢾</td>
<td>⢦</td>
<td>⢬</td>
<td>⢴</td>
<td>⢶</td>
<td>⢼</td>
</tr>


<tr>
<td>368</td>
<td>1368</td>
<td>12368</td>
<td>13468</td>
<td>134568</td>
<td>13568</td>
<td>123468</td>
<td>1234568</td>
<td>123568</td>
<td>23468</td>
<td>234568</td>
<td>2368</td>
<td>3468</td>
<td>3568</td>
<td>23568</td>
<td>34568</td>
</tr>


<tr>
<td>calt</td>
<td>&#xa0;</td>
<td>½</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>é</td>
<td>à</td>
<td>&#xa0;</td>
<td>ù</td>
<td>Ü</td>
<td>ò</td>
<td>&#xa0;</td>
<td>csalt</td>
<td>ß</td>
</tr>
</tbody>

<tbody>
<tr>
<td>⢠</td>
<td>⢡</td>
<td>⢣</td>
<td>⢩</td>
<td>⢹</td>
<td>⢱</td>
<td>⢫</td>
<td>⢻</td>
<td>⢳</td>
<td>⢪</td>
<td>⢺</td>
<td>⢢</td>
<td>⢨</td>
<td>⢰</td>
<td>⢲</td>
<td>⢸</td>
</tr>


<tr>
<td>68</td>
<td>168</td>
<td>1268</td>
<td>1468</td>
<td>14568</td>
<td>1568</td>
<td>12468</td>
<td>124568</td>
<td>12568</td>
<td>2468</td>
<td>24568</td>
<td>268</td>
<td>468</td>
<td>568</td>
<td>2568</td>
<td>4568</td>
</tr>


<tr>
<td>alt</td>
<td>&#xa0;</td>
<td>„</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>ü</td>
<td>ö</td>
<td>&#xa0;</td>
<td>salt</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>&#xa0;</td>
<td>°</td>
</tr>
</tbody>
</table>

# Modifier

Modifier keys and Mode switches are active for the next entry.

Double entry of Modifier keys and Mode switches activates the modes for all next entries.

Double entry of Modifier keys and Mode switches deactivates modes.

-   Modifier keys  
    -   Control  
        3-8-space
    -   Alt  
        6-8-space
    -   Shift  
        7-8-space
-   Mode switches
    -   Switch between Navigation and Braille  
        4-8-space
    -   Reset   
        enables Navigation mode and disables all other modes   
        2-8-space
    -   Function keys F1-F12  
        1-8-space  
        followed by letter a to l

