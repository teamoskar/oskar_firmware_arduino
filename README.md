# oskar_firmware_arduino
oskar_firmware_arduino is the firmware for Oskar, based on [Arduino Micro](https://store.arduino.cc/arduino-micro).

Oskar is a open-source, mobile braille-keyboard.

The arrangement of 8 keys in a braille cell block and two additional keys allows Oskar Zither to be controlled without a supporting surface.

# Chords

The 8 keys in the braille cell block correspond to the 8 dots in the braille alphabet. The 8 keys are operated with the fingers. Combined pressing of the keys results in the braille character input. The two additional keys on the shoulders can be operated with the thumbs and are used to enter spaces and additional key combinations, for example, navigation commands.

- [Erich Schmids 10 Keys Braille](brailletable.md)

# Build

## Need
- Software [Arduino IDE 1.8.13](https://www.arduino.cc/en/software),
- Hardware [Arduino Micro](https://store.arduino.cc/arduino-micro)

## Preparation

Move the oskar_firmware_arduino projectdirectory in your Arduino scatchbook folder ("/home/user/Arduino").

Move the libraries from your projectdirctory/libraries to scatchbook/libraries ("/home/user/Arduino/libraries/").

## Arduino Software IDE

[getting Started with Arduino Software IDE](https://www.arduino.cc/en/Guide/Environment)

Open the new oskar_firmware_arduino sketchbook in Arduino.

Select Board "Arduino Micro" then Verify/Compile.

Connect your Arduino Micro via USB-cable and Upload.

# Contact
[https://oskars.org](https://oskars.org)
Johannes Strelka-Petz <johannes_at_oskars.org>

# Copyright & License
    SPDX-FileCopyrightText: 2020 Johannes Strelka-Petz <johannes_at_oskars.org>
    SPDX-License-Identifier: GPL-3.0-or-later

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.