#                                                                                  \
   SPDX-FileCopyrightText: 2020 Johannes Strelka-Petz <johannes_at_oskar.ddns.mobi>\
   SPDX-License-Identifier: GPL-3.0-or-later                                       \
                                                                                   \
   Makefile                                                                        \
                                                                                   \
   Copyright 2020 Johannes Strelka-Petz <johannes@oskar.ddns.mobi>                 \
                                                                                   \
   This file is part of Oskar.                                                     \
                                                                                   \
   Oskar is free software: you can redistribute it and/or modify                   \
   it under the terms of the GNU General Public License as published by            \
   the Free Software Foundation, either version 3 of the License, or               \
   (at your option) any later version.                                             \
                                                                                   \
   Oskar is distributed in the hope that it will be useful,                        \
   but WITHOUT ANY WARRANTY; without even the implied warranty of                  \
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   \
   GNU General Public License for more details.                                    \
                                                                                   \
   You should have received a copy of the GNU General Public License               \
   along with Oskar.  If not, see <https://www.gnu.org/licenses/>.                 \
                                                                                   \

# ARDUINO_DIR   = /usr/share/arduino
ARDUINO_DIR   = /opt/arduino-1.8.13
ARDMK_DIR     = /usr/share/arduino
AVR_TOOLS_DIR = /opt/arduino-1.8.13/hardware/tools/avr

BOARD_TAG    = micro
ROOT_DIR:=$(realpath $(dir $(realpath $(lastword $(MAKEFILE_LIST)))))
USER_LIB_PATH = $(realpath $(ROOT_DIR)/libraries)
ARDUINO_LIBS = Keyboard HID braille_german



include $(ARDMK_DIR)/Arduino.mk
