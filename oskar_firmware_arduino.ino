/*
   SPDX-FileCopyrightText: 2020 Johannes Strelka-Petz <johannes_at_oskar.ddns.mobi>
   SPDX-License-Identifier: GPL-3.0-or-later

   main.cpp

   Copyright 2020 Johannes Strelka-Petz <johannes@oskar.ddns.mobi>

   This file is part of Oskar.

   Oskar is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Oskar is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Oskar.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Keyboard.h"
#include "braille_german.h"

const int dot1Pin = 2;
const int dot4Pin = 4;
const int dot2Pin = 3;
const int dot5Pin = 5;
const int dot3Pin = 6;
const int dot6Pin = 7;
const int dot8Pin = 8;
const int dot7Pin = 9;
const int dot9Pin = 15; //SCK thumb chord (expt 2 8)256
const int dot10Pin = 16; //MOSI thumb chord (expt 2 9)512


const int dotPins[]={dot1Pin, dot2Pin, dot3Pin, dot4Pin, dot5Pin, dot6Pin, dot7Pin, dot8Pin, dot9Pin, dot10Pin};
const int dotPinsSize=10;

//Braille_ Braille();

short chord_prior=0;
bool chord_was_greater_then_chord_prior=false;

uint16_t sticky_modifier=0;
uint8_t modi=0;
uint16_t modi_modifier=0xE000;

short sticky_dot=0;
short modi_dot=0;

// default keymap space1 (navigation)
short sticky_keymap=kc_keymap_space1;
short modi_keymap=kc_keymap_space1;

KeyReport _keyReport;

boolean debug=false;

void setup() {
  if (debug)
    {
      Serial.begin(9600);
    }

  for(int i=0; i<dotPinsSize;i++)
    {
      pinMode(dotPins[i], INPUT_PULLUP);
    }
  // initialize control over the keyboard:
  Keyboard.begin();
}

short chord_present()
{
  short chord_present=0;
  for(int i=0; i<dotPinsSize;i++)
    {
      if(digitalRead(dotPins[i])==LOW){chord_present=(chord_present | (1<<i));}
    }
  return chord_present;
}

short chord_last()
{
  short chord = chord_present();
  if (chord < chord_prior)
    {// less keys in chord then in previous chord
      if (chord_was_greater_then_chord_prior)
	{// there was chord composition
	  chord_was_greater_then_chord_prior=false; //end of chord composition, start of chord decomposition
	  return chord_prior;
	}else
	{// chord decomposition
	  chord_prior=chord;
	}
    } else
    {
      if (chord > chord_prior)
	{// more keys in chord then in previous chord
	  chord_was_greater_then_chord_prior=true; //start of chord composition, end of chord decomposition
	  chord_prior=chord;
	  delay(10);
	}
    }
  return false;
}

void sendReport(KeyReport* keys)
{
  HID().SendReport(2,keys,sizeof(KeyReport));
}

void write(short chord)
{
  if (!keycodes(chord,kc_deadkey,sticky_keymap)==0)
    {
      _keyReport.keys[0] = keycodes(chord, kc_deadkey, sticky_keymap);
      _keyReport.keys[1] = 0;
      _keyReport.keys[2] = 0;
      _keyReport.keys[3] = 0;
      _keyReport.keys[4] = 0;
      _keyReport.keys[5] = 0;
      _keyReport.modifiers = keycodes(chord, kc_deadkey_modifier, sticky_keymap);
      sendReport(&_keyReport);
      Keyboard.releaseAll();      
    }
  
  _keyReport.keys[0] = keycodes(chord | sticky_dot, kc_char, sticky_keymap);
  _keyReport.keys[1] = 0;
  _keyReport.keys[2] = 0;
  _keyReport.keys[3] = 0;
  _keyReport.keys[4] = 0;
  _keyReport.keys[5] = 0;
  _keyReport.modifiers = keycodes(chord | sticky_dot, kc_char_modifier, sticky_keymap) | sticky_modifier;
  sendReport(&_keyReport);
  Keyboard.releaseAll();

  if (debug)
    {
      delay(2);
      // if (!(modi_modifier==0xE000))
      // 	{
      // 	  Serial.print(modi_modifier);
      // 	}
      if (!(modi_dot==0))
	{
	  Serial.println(sticky_dot);
	  Serial.println(modi_modifier);
	}
    }
}

void change_mode(short chord, short modifierchord, uint16_t modifierkey){
  if (chord & modifierchord)
    {
      if ((sticky_modifier & modifierkey) == modifierkey)
	{
	  if (modi & modifierchord)
	    // sticky 1, modi 1 -> sticky 0, modi 1
	    {
	      sticky_modifier = sticky_modifier ^ (modifierkey & B11111111);
	      if (debug){Serial.print("s1,m1->s0,m1 ");}
	    }
	  else
	    {
	      // sticky 1, modi 0 -> sticky 1, modi 1
	      modi = modi ^ modifierchord;
	      modi_modifier = modi_modifier | modifierkey;
	      if (debug){Serial.print("s1,m0->s1,m1 ");}
	    }
	}
      else if (modi & modifierchord)
	// sticky 0, modi 1 -> sticky 0, modi 0
	{
	  modi = modi ^ modifierchord;
	  modi_modifier = modi_modifier ^ (modifierkey & B11111111);
	  if (debug){Serial.print("s0,m1->s0,m0 ");}
	}
      else
	// sticky 0, modi 0 -> sticky 1, modi 0
	{
	  sticky_modifier = sticky_modifier | modifierkey;
	  if (debug){Serial.print("s0,m0->s1,m0 ");}
	}
    }
}

void change_mode_dot(short chord, short modifierchord, short modifierdot){
  if (chord & modifierchord)
    {
      if ((sticky_dot & modifierdot) == modifierdot)
	{
	  if (modi & modifierchord)
	    // sticky 1, modi 1 -> sticky 0, modi 1
	    {
	      sticky_dot = sticky_dot ^ modifierdot;
	      if (debug){Serial.print("s1,m1->s0,m1 ");}
	    }
	  else
	    {
	      // sticky 1, modi 0 -> sticky 1, modi 1
	      modi = modi ^ modifierchord;
	      modi_dot = modi_dot | modifierdot;
	      if (debug){Serial.print("s1,m0->s1,m1 ");}
	    }
	}
      else if (modi & modifierchord)
	// sticky 0, modi 1 -> sticky 0, modi 0
	{
	  modi = modi ^ modifierchord;
	  modi_dot = modi_dot ^ modifierdot;
	  if (debug){Serial.print("s0,m1->s0,m0 ");}
	}
      else
	// sticky 0, modi 0 -> sticky 1, modi 0
	{
	  sticky_dot = sticky_dot | modifierdot;
	  if (debug){Serial.print("s0,m0->s1,m0 ");}
	}
    }
}

void change_mode_keymap(short chord, short modifierchord, short modifierkeymap){
  if (chord & modifierchord)
    {
      if (debug){Serial.print("modifierkeymap ");}
      if (sticky_keymap == modifierkeymap)
	{
	  if (modi & modifierchord)
	    // sticky 1, modi 1 -> sticky 0, modi 1
	    {
	      sticky_keymap = kc_keymap_std;
	      if (debug){Serial.print("s1,m1->s0,m1 ");}
	    }
	  else
	    {
	      // sticky 1, modi 0 -> sticky 1, modi 1
	      modi = modi ^ modifierchord;
	      modi_keymap =  modifierkeymap;
	      if (debug){Serial.print("s1,m0->s1,m1 ");}
	    }
	}
      else if (modi & modifierchord)
	// sticky 0, modi 1 -> sticky 0, modi 0
	{
	  modi = modi ^ modifierchord;
	  modi_keymap = kc_keymap_std;
	  if (debug){Serial.print("s0,m1->s0,m0 ");}
	}
      else
	// sticky 0, modi 0 -> sticky 1, modi 0
	{
	  sticky_keymap = modifierkeymap;
	  if (debug){Serial.print("s0,m0->s1,m0 ");}
	}
    }
}

void change_mode_reset(short chord, short modifierchord){
  if (chord & modifierchord)
    {
      sticky_modifier=0;
      modi=0;
      modi_modifier=0xE000;

      sticky_dot=0;
      modi_dot=0;

      // default keymap space1 (navigation)
      sticky_keymap=kc_keymap_space1;
      modi_keymap=kc_keymap_space1;
    }
}
void modes_n_write(short chord)
{
  // modifier chords are chords with 8 and thumbs (9 or 10)
  if (chord & 1<<(8-1) && (chord & 1<<(9-1) || (chord & 1<<(10-1))))
    // change modes
    {
      // change_mode__(chord pressed, key required, modifier action)
      change_mode_keymap(chord, (1<<(1-1)), kc_keymap_function);
      change_mode_reset(chord, (1<<(2-1)));
      change_mode_keymap(chord, (1<<(4-1)), kc_keymap_space1);
      change_mode_dot(chord, (1<<(5-1)), (1<<(9-1)));
      change_mode(chord, (1<<(3-1)), MODIFIERKEY_CTRL);
      change_mode(chord, (1<<(6-1)), MODIFIERKEY_LEFT_ALT);
      change_mode(chord, (1<<(7-1)), MODIFIERKEY_LEFT_SHIFT);
      if (!(chord & 1<<(3-1) || chord & 1<<(6-1) || chord & 1<<(7-1)))
	{
	  sticky_modifier = 0;
	}
    }
  else
    {
      // write 
      write(chord);
      // and do not change modes but set sticky_modes to current modes
      sticky_modifier=modi_modifier;
      sticky_dot=modi_dot;
      sticky_keymap=modi_keymap;
    }
}

void loop() {
  short chord=0;
  chord=chord_last();
  if(chord)
    {
      modes_n_write(chord);
      delay(50);
    }
}
