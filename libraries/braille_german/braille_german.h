/*
   SPDX-FileCopyrightText: 2020 Johannes Strelka-Petz <johannes_at_oskar.ddns.mobi>
   SPDX-License-Identifier: GPL-3.0-or-later

   braille_german.h

   Copyright 2020 Johannes Strelka-Petz <johannes@oskar.ddns.mobi>

   This file is part of Oskar.

   Oskar is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Oskar is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Oskar.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BRAILLE_GERMAN_h
#define BRAILLE_GERMAN_h

/* #define KEY_A 0x61 */
/* #define KEY_COMMA 0x2c */
/* #define KEY_B 0x62 */
/* #define KEY_PERIOD 0x2e */

#ifndef MODIFIERKEY_CTRL
#define MODIFIERKEY_CTRL        ( 0x01 | 0xE000 )
#endif
#ifndef MODIFIERKEY_SHIFT 
#define MODIFIERKEY_SHIFT       ( 0x02 | 0xE000 )
#endif
#ifndef MODIFIERKEY_ALT 
#define MODIFIERKEY_ALT         ( 0x04 | 0xE000 )
#endif
#ifndef MODIFIERKEY_GUI 
#define MODIFIERKEY_GUI         ( 0x08 | 0xE000 )
#endif
#ifndef MODIFIERKEY_LEFT_CTRL 
#define MODIFIERKEY_LEFT_CTRL   ( 0x01 | 0xE000 )
#endif
#ifndef MODIFIERKEY_LEFT_SHIFT 
#define MODIFIERKEY_LEFT_SHIFT  ( 0x02 | 0xE000 )
#endif
#ifndef MODIFIERKEY_LEFT_ALT 
#define MODIFIERKEY_LEFT_ALT    ( 0x04 | 0xE000 )
#endif
#ifndef MODIFIERKEY_LEFT_GUI 
#define MODIFIERKEY_LEFT_GUI    ( 0x08 | 0xE000 )
#endif
#ifndef MODIFIERKEY_RIGHT_CTRL 
#define MODIFIERKEY_RIGHT_CTRL  ( 0x10 | 0xE000 )
#endif
#ifndef MODIFIERKEY_RIGHT_SHIFT 
#define MODIFIERKEY_RIGHT_SHIFT ( 0x20 | 0xE000 )
#endif
#ifndef MODIFIERKEY_RIGHT_ALT 
#define MODIFIERKEY_RIGHT_ALT   ( 0x40 | 0xE000 )
#endif
#ifndef MODIFIERKEY_RIGHT_GUI 
#define MODIFIERKEY_RIGHT_GUI   ( 0x80 | 0xE000 )
#endif
 
#ifndef KEY_SYSTEM_POWER_DOWN  
#define KEY_SYSTEM_POWER_DOWN   ( 0x81 | 0xE200 )
#endif
#ifndef KEY_SYSTEM_SLEEP 
#define KEY_SYSTEM_SLEEP        ( 0x82 | 0xE200 )
#endif
#ifndef KEY_SYSTEM_WAKE_UP 
#define KEY_SYSTEM_WAKE_UP      ( 0x83 | 0xE200 )
#endif

#ifndef KEY_MEDIA_PLAY 
#define KEY_MEDIA_PLAY          ( 0xB0 | 0xE400 )
#endif
#ifndef KEY_MEDIA_PAUSE 
#define KEY_MEDIA_PAUSE         ( 0xB1 | 0xE400 )
#endif
#ifndef KEY_MEDIA_RECORD 
#define KEY_MEDIA_RECORD        ( 0xB2 | 0xE400 )
#endif
#ifndef KEY_MEDIA_FAST_FORWARD 
#define KEY_MEDIA_FAST_FORWARD  ( 0xB3 | 0xE400 )
#endif
#ifndef KEY_MEDIA_REWIND 
#define KEY_MEDIA_REWIND        ( 0xB4 | 0xE400 )
#endif
#ifndef KEY_MEDIA_NEXT_TRACK 
#define KEY_MEDIA_NEXT_TRACK    ( 0xB5 | 0xE400 )
#endif
#ifndef KEY_MEDIA_PREV_TRACK 
#define KEY_MEDIA_PREV_TRACK    ( 0xB6 | 0xE400 )
#endif
#ifndef KEY_MEDIA_STOP 
#define KEY_MEDIA_STOP          ( 0xB7 | 0xE400 )
#endif
#ifndef KEY_MEDIA_EJECT 
#define KEY_MEDIA_EJECT         ( 0xB8 | 0xE400 )
#endif
#ifndef KEY_MEDIA_RANDOM_PLAY 
#define KEY_MEDIA_RANDOM_PLAY   ( 0xB0 | 0xE400 )
#endif
#ifndef KEY_MEDIA_PLAY_PAUSE 
#define KEY_MEDIA_PLAY_PAUSE    ( 0xCD | 0xE400 )
#endif
#ifndef KEY_MEDIA_PLAY_SKIP 
#define KEY_MEDIA_PLAY_SKIP     ( 0xCE | 0xE400 )
#endif
#ifndef KEY_MEDIA_MUTE 
#define KEY_MEDIA_MUTE          ( 0xE2 | 0xE400 )
#endif
#ifndef KEY_MEDIA_VOLUME_INC 
#define KEY_MEDIA_VOLUME_INC    ( 0xE9 | 0xE400 )
#endif
#ifndef KEY_MEDIA_VOLUME_DEC 
#define KEY_MEDIA_VOLUME_DEC    ( 0xEA | 0xE400 )
#endif

// #define KEY_A                   (   4  | 0xF000 )
#define KEY_A                   0xF004
#define KEY_B                   (   5  | 0xF000 )
#define KEY_C                   (   6  | 0xF000 )
#define KEY_D                   (   7  | 0xF000 )
#define KEY_E                   (   8  | 0xF000 )
#define KEY_F                   (   9  | 0xF000 )
#define KEY_G                   (  10  | 0xF000 )
#define KEY_H                   (  11  | 0xF000 )
#define KEY_I                   (  12  | 0xF000 )
#define KEY_J                   (  13  | 0xF000 )
#define KEY_K                   (  14  | 0xF000 )
#define KEY_L                   (  15  | 0xF000 )
#define KEY_M                   (  16  | 0xF000 )
#define KEY_N                   (  17  | 0xF000 )
#define KEY_O                   (  18  | 0xF000 )
#define KEY_P                   (  19  | 0xF000 )
#define KEY_Q                   (  20  | 0xF000 )
#define KEY_R                   (  21  | 0xF000 )
#define KEY_S                   (  22  | 0xF000 )
#define KEY_T                   (  23  | 0xF000 )
#define KEY_U                   (  24  | 0xF000 )
#define KEY_V                   (  25  | 0xF000 )
#define KEY_W                   (  26  | 0xF000 )
#define KEY_X                   (  27  | 0xF000 )
#define KEY_Y                   (  28  | 0xF000 )
#define KEY_Z                   (  29  | 0xF000 )
#define KEY_1                   (  30  | 0xF000 )
#define KEY_2                   (  31  | 0xF000 )
#define KEY_3                   (  32  | 0xF000 )
#define KEY_4                   (  33  | 0xF000 )
#define KEY_5                   (  34  | 0xF000 )
#define KEY_6                   (  35  | 0xF000 )
#define KEY_7                   (  36  | 0xF000 )
#define KEY_8                   (  37  | 0xF000 )
#define KEY_9                   (  38  | 0xF000 )
#define KEY_0                   (  39  | 0xF000 )
#ifndef KEY_ENTER 
#define KEY_ENTER               (  40  | 0xF000 )
#endif
//#ifndef KEY_ESC
#undef KEY_ESC
#define KEY_ESC                 (  41  | 0xF000 )
//#endif

//#ifdef KEY_BACKSPACE 
#undef KEY_BACKSPACE
//#endif
#define KEY_BACKSPACE           (  42  | 0xF000 )

//#ifndef KEY_TAB
#undef KEY_TAB
#define KEY_TAB                 (  43  | 0xF000 )
//#endif
#ifndef KEY_SPACE 
#define KEY_SPACE               (  44  | 0xF000 )
#endif
#ifndef KEY_MINUS 
#define KEY_MINUS               (  45  | 0xF000 )
#endif
#ifndef KEY_EQUAL 
#define KEY_EQUAL               (  46  | 0xF000 )
#endif
#ifndef KEY_LEFT_BRACE 
#define KEY_LEFT_BRACE          (  47  | 0xF000 )
#endif
#ifndef KEY_RIGHT_BRACE 
#define KEY_RIGHT_BRACE         (  48  | 0xF000 )
#endif
#ifndef KEY_BACKSLASH 
#define KEY_BACKSLASH           (  49  | 0xF000 )
#endif
#ifndef KEY_NON_US_NUM 
#define KEY_NON_US_NUM          (  50  | 0xF000 )
#endif
#ifndef KEY_SEMICOLON 
#define KEY_SEMICOLON           (  51  | 0xF000 )
#endif
#ifndef KEY_QUOTE 
#define KEY_QUOTE               (  52  | 0xF000 )
#endif
#ifndef KEY_TILDE 
#define KEY_TILDE               (  53  | 0xF000 )
#endif
#ifndef KEY_COMMA 
#define KEY_COMMA               (  54  | 0xF000 )
#endif
#ifndef KEY_PERIOD 
#define KEY_PERIOD              (  55  | 0xF000 )
#endif
#ifndef KEY_SLASH 
#define KEY_SLASH               (  56  | 0xF000 )
#endif
#ifndef KEY_CAPS_LOCK 
#define KEY_CAPS_LOCK           (  57  | 0xF000 )
#endif
//#ifndef KEY_F1
#undef KEY_F1
#define KEY_F1                  (  58  | 0xF000 )
//#endif
//#ifndef KEY_F2
#undef KEY_F2
#define KEY_F2                  (  59  | 0xF000 )
//#endif
//#ifndef KEY_F3
#undef KEY_F3
#define KEY_F3                  (  60  | 0xF000 )
//#endif
//#ifndef KEY_F4
#undef KEY_F4
#define KEY_F4                  (  61  | 0xF000 )
//#endif
//#ifndef KEY_F5
#undef KEY_F5
#define KEY_F5                  (  62  | 0xF000 )
//#endif
//#ifndef KEY_F6
#undef KEY_F6
#define KEY_F6                  (  63  | 0xF000 )
//#endif
//#ifndef KEY_F7
#undef KEY_F7
#define KEY_F7                  (  64  | 0xF000 )
//#endif
//#ifndef KEY_F8
#undef KEY_F8
#define KEY_F8                  (  65  | 0xF000 )
//#endif
//#ifndef KEY_F9
#undef KEY_F9
#define KEY_F9                  (  66  | 0xF000 )
//#endif
//#ifndef KEY_F10
#undef KEY_F10
#define KEY_F10                 (  67  | 0xF000 )
//#endif
//#ifndef KEY_F11
#undef KEY_F11
#define KEY_F11                 (  68  | 0xF000 )
//#endif
//#ifndef KEY_F12
#undef KEY_F12
#define KEY_F12                 (  69  | 0xF000 )
//#endif
#ifndef KEY_PRINTSCREEN 
#define KEY_PRINTSCREEN         (  70  | 0xF000 )
#endif
#ifndef KEY_SCROLL_LOCK 
#define KEY_SCROLL_LOCK         (  71  | 0xF000 )
#endif
#ifndef KEY_PAUSE 
#define KEY_PAUSE               (  72  | 0xF000 )
#endif
//#ifndef KEY_INSERT
#undef KEY_INSERT
#define KEY_INSERT              (  73  | 0xF000 )
//#endif
//#ifndef KEY_HOME
#undef KEY_HOME 
#define KEY_HOME                (  74  | 0xF000 )
//#endif
//#ifndef KEY_PAGE_UP
#undef KEY_PAGE_UP
#define KEY_PAGE_UP             (  75  | 0xF000 )
//#endif
//#ifndef KEY_DELETE 
#undef KEY_DELETE
#define KEY_DELETE              (  76  | 0xF000 )
//#endif
//#ifndef KEY_END
#undef KEY_END
#define KEY_END                 (  77  | 0xF000 )
//#endif
//#ifndef KEY_PAGE_DOWN 
#undef KEY_PAGE_DOWN
#define KEY_PAGE_DOWN           (  78  | 0xF000 )
//#endif
#ifndef KEY_RIGHT 
#define KEY_RIGHT               (  79  | 0xF000 )
#endif
#ifndef KEY_LEFT 
#define KEY_LEFT                (  80  | 0xF000 )
#endif
#ifndef KEY_DOWN 
#define KEY_DOWN                (  81  | 0xF000 )
#endif
#ifndef KEY_UP 
#define KEY_UP                  (  82  | 0xF000 )
#endif
#ifndef KEY_NUM_LOCK 
#define KEY_NUM_LOCK            (  83  | 0xF000 )
#endif
#ifndef KEYPAD_SLASH 
#define KEYPAD_SLASH            (  84  | 0xF000 )
#endif
#ifndef KEYPAD_ASTERIX 
#define KEYPAD_ASTERIX          (  85  | 0xF000 )
#endif
#ifndef KEYPAD_MINUS 
#define KEYPAD_MINUS            (  86  | 0xF000 )
#endif
#ifndef KEYPAD_PLUS 
#define KEYPAD_PLUS             (  87  | 0xF000 )
#endif
#ifndef KEYPAD_ENTER 
#define KEYPAD_ENTER            (  88  | 0xF000 )
#endif
#ifndef KEYPAD_1 
#define KEYPAD_1                (  89  | 0xF000 )
#endif
#ifndef KEYPAD_2 
#define KEYPAD_2                (  90  | 0xF000 )
#endif
#ifndef KEYPAD_3 
#define KEYPAD_3                (  91  | 0xF000 )
#endif
#ifndef KEYPAD_4 
#define KEYPAD_4                (  92  | 0xF000 )
#endif
#ifndef KEYPAD_5 
#define KEYPAD_5                (  93  | 0xF000 )
#endif
#ifndef KEYPAD_6 
#define KEYPAD_6                (  94  | 0xF000 )
#endif
#ifndef KEYPAD_7 
#define KEYPAD_7                (  95  | 0xF000 )
#endif
#ifndef KEYPAD_8 
#define KEYPAD_8                (  96  | 0xF000 )
#endif
#ifndef KEYPAD_9 
#define KEYPAD_9                (  97  | 0xF000 )
#endif
#ifndef KEYPAD_0 
#define KEYPAD_0                (  98  | 0xF000 )
#endif
#ifndef KEYPAD_PERIOD 
#define KEYPAD_PERIOD           (  99  | 0xF000 )
#endif
#ifndef KEY_NON_US_BS 
#define KEY_NON_US_BS           ( 100  | 0xF000 )
#endif
#ifndef KEY_MENU 
#define KEY_MENU        	( 101  | 0xF000 )
#endif
#ifndef KEY_F13 
#define KEY_F13                 ( 104  | 0xF000 )
#endif
#ifndef KEY_F14 
#define KEY_F14                 ( 105  | 0xF000 )
#endif
#ifndef KEY_F15 
#define KEY_F15                 ( 106  | 0xF000 )
#endif
#ifndef KEY_F16 
#define KEY_F16                 ( 107  | 0xF000 )
#endif
#ifndef KEY_F17 
#define KEY_F17                 ( 108  | 0xF000 )
#endif
#ifndef KEY_F18 
#define KEY_F18                 ( 109  | 0xF000 )
#endif
#ifndef KEY_F19 
#define KEY_F19                 ( 110  | 0xF000 )
#endif
#ifndef KEY_F20 
#define KEY_F20                 ( 111  | 0xF000 )
#endif
#ifndef KEY_F21 
#define KEY_F21                 ( 112  | 0xF000 )
#endif
#ifndef KEY_F22 
#define KEY_F22                 ( 113  | 0xF000 )
#endif
#ifndef KEY_F23 
#define KEY_F23                 ( 114  | 0xF000 )
#endif
#ifndef KEY_F24 
#define KEY_F24                 ( 115  | 0xF000 )
#endif

// for compatibility with Leonardo's slightly different names
#ifndef KEY_UP_ARROW 
#define KEY_UP_ARROW	KEY_UP
#endif
#ifndef KEY_DOWN_ARROW 
#define KEY_DOWN_ARROW	KEY_DOWN
#endif
#ifndef KEY_LEFT_ARROW 
#define KEY_LEFT_ARROW	KEY_LEFT
#endif
#ifndef KEY_RIGHT_ARROW 
#define KEY_RIGHT_ARROW	KEY_RIGHT
#endif
#ifndef KEY_RETURN 
#define KEY_RETURN	KEY_ENTER
#endif
#ifndef KEY_LEFT_CTRL 
#define KEY_LEFT_CTRL	MODIFIERKEY_LEFT_CTRL
#endif
#ifndef KEY_LEFT_SHIFT 
#define KEY_LEFT_SHIFT	MODIFIERKEY_LEFT_SHIFT
#endif
#ifndef KEY_LEFT_ALT 
#define KEY_LEFT_ALT	MODIFIERKEY_LEFT_ALT
#endif
#ifndef KEY_LEFT_GUI 
#define KEY_LEFT_GUI	MODIFIERKEY_LEFT_GUI
#endif
#ifndef KEY_RIGHT_CTRL 
#define KEY_RIGHT_CTRL	MODIFIERKEY_RIGHT_CTRL
#endif
#ifndef KEY_RIGHT_SHIFT 
#define KEY_RIGHT_SHIFT	MODIFIERKEY_RIGHT_SHIFT
#endif
#ifndef KEY_RIGHT_ALT 
#define KEY_RIGHT_ALT	MODIFIERKEY_RIGHT_ALT
#endif
#ifndef KEY_RIGHT_GUI 
#define KEY_RIGHT_GUI	MODIFIERKEY_RIGHT_GUI
#endif


#ifndef KEY_LT
#define KEY_LT 0x64
#endif

#ifndef SHIFT_MASK
#define SHIFT_MASK		0x0040
#endif
#ifndef ALTGR_MASK
#define ALTGR_MASK		0x0080
#endif
#ifndef DEADKEYS_MASK
#define DEADKEYS_MASK		0x0700
#endif
#ifndef CIRCUMFLEX_BITS
#define	CIRCUMFLEX_BITS		0x0300
#endif
#ifndef ACUTE_ACCENT_BITS
#define ACUTE_ACCENT_BITS	0x0400
#endif
#ifndef GRAVE_ACCENT_BITS
#define GRAVE_ACCENT_BITS	0x0500
#endif
#ifndef KEYCODE_TYPE
#define KEYCODE_TYPE		uint16_t
#endif
#ifndef KEYCODE_MASK
#define KEYCODE_MASK		0x07FF
#endif
#ifndef DEADKEY_CIRCUMFLEX
#define DEADKEY_CIRCUMFLEX	KEY_TILDE
#endif
#ifndef DEADKEY_ACUTE_ACCENT
#define DEADKEY_ACUTE_ACCENT	KEY_EQUAL
#endif
#ifndef DEADKEY_GRAVE_ACCENT
#define DEADKEY_GRAVE_ACCENT	KEY_EQUAL + SHIFT_MASK
#endif
#ifndef KEY_NON_US_100
#define KEY_NON_US_100		63
#endif

#define kc_char 0
#define kc_char_modifier 1
#define kc_deadkey 2
#define kc_deadkey_modifier 3

#define kc_keymap_std 0
#define kc_keymap_space1 1
#define kc_keymap_space2 2
#define kc_keymap_function 3

uint16_t keycodes(short chord, short kc_field, short keymap);

#endif
